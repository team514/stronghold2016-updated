/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.ShootBall;
import frc.robot.subsystems.BallUtil;
import frc.robot.subsystems.DriveUtil;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DriveUtil driveUtil = new DriveUtil();
  private final BallUtil ballUtil = new BallUtil();

  Joystick driver, controller;  

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();

    driver = new Joystick(0);
    controller = new Joystick(1);
    
    driveUtil.setDefaultCommand(new RunCommand(() -> {
      double leftJoystick = driver.getY();
      double rightJoystick = driver.getZ();
      driveUtil.driveTank(leftJoystick, rightJoystick);
    }, driveUtil));

    ballUtil.setDefaultCommand(new RunCommand(() -> {
      ballUtil.setMagMotor(0.0);
    }, ballUtil));

    JoystickButton shoot = new JoystickButton (driver, 1);
    shoot.whileHeld(new ShootBall(1.0*Constants.STEAM_NITE_LIMIT, ballUtil));
    JoystickButton intake = new JoystickButton (driver, 2);
    intake.whileHeld(new ShootBall(-1.0, ballUtil));
    JoystickButton changeDriveMode = new JoystickButton (driver, 2);
    changeDriveMode.whenPressed(new InstantCommand(() -> {
      driveUtil.setDriveMode(!driveUtil.getDriveMode());
      }));
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
  }


  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return null;
  }

  public double getDriverLeftX(){
    return driver.getX(Hand.kLeft);
  }

  public double getDriverLeftY(){
    return driver.getY(Hand.kLeft);
  }

  public double getDriverRightX(){
    return driver.getX(Hand.kRight);
  }

  public double getDriverRightY(){
    return driver.getY(Hand.kRight);
  }
}
