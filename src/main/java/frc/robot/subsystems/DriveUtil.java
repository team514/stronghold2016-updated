/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveUtil extends SubsystemBase {
  Boolean tankMode = true;
  WPI_TalonSRX leftMaster, leftSlave, rightMaster, rightSlave; 

  /**
   * Creates a new DriveUtil.
   */
  public DriveUtil() {
    leftMaster = new WPI_TalonSRX(1);
    rightMaster = new WPI_TalonSRX(3);
    
    leftSlave = new WPI_TalonSRX(2);
    leftSlave.follow(leftMaster);

    rightSlave = new WPI_TalonSRX(4);
    rightSlave.follow(rightMaster);

    leftMaster.setInverted(true);
    leftSlave.setInverted(true);
  }

  public void driveTank(double left, double right) {
    leftMaster.set(left);
    rightMaster.set(right);
  }

  public void driveArcade(double xAxis, double yAxis) {
    double left, right;
    left = yAxis - xAxis;
    right = yAxis + xAxis;
    driveTank(left*Constants.STEAM_NITE_LIMIT, right*Constants.STEAM_NITE_LIMIT);
  }

  public boolean getDriveMode() {
    return tankMode;
  }

  public void setDriveMode(boolean driveMode) {
    tankMode = driveMode;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
