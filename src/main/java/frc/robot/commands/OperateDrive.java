/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.DriveUtil;

public class OperateDrive extends CommandBase {
  /**
   * Creates a new OperateDrive.
   */
  DriveUtil driveUtil;
  public OperateDrive(final DriveUtil driveUtil) {
    this.driveUtil = driveUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
  }
  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (driveUtil.getDriveMode()) {
      // Tank Mode
      final double leftY = Robot.robotContainer.getDriverLeftY();
      final double rightY = Robot.robotContainer.getDriverRightY();
      driveUtil.driveTank(leftY, rightY);
    } else {
      // Arcade Mode
      final double leftX = Robot.robotContainer.getDriverLeftX();
      final double leftY = Robot.robotContainer.getDriverLeftY();
      driveUtil.driveArcade(leftX, leftY);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(final boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
