/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallUtil;

public class ShootBall extends CommandBase {
  /**
   * Creates a new ShootBall.
   */
  BallUtil ballUtil;
  double speed;
  public ShootBall(double speed, BallUtil ballUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.ballUtil = ballUtil;
    this.speed = speed;
    addRequirements(ballUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ballUtil.setMagMotor(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
